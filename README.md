Sample Linux character device driver.

To build and cleanup for local system:

    make
    make clean

To cross compile:

    make XCC=<path to cross compiler, sans "gcc" suffix> MODDIR=<path to linux source tree> ARCH=<arm, mips, etc.>
    make XCC=<path to cross compiler, sans "gcc" suffix> MODDIR=<path to linux source tree> ARCH=<arm, mips, etc.> clean
