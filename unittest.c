#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/* Include the driver module public header */
#include "mjh.h"

/* Where we find the major number for our device */
#define MAJOR_NUM_PATH  "/sys/module/mjh/parameters/majorNumber"

FILE    *fd;
char    buf[256];
char    device_name[256];
int     cnt = 0;
int     majorNumber = -1;

/*
 * Read from the device and print out the result.
 */
void readMajorNumber()
{
    FILE    *fp;

    fp = fopen(MAJOR_NUM_PATH, "r");
    if ( fp == NULL )
    {
        printf("Failed to get major number path.  Ioctl tests will be skipped.\n");
        return;
    }
    fread(buf, 256, 1, fp);
    fclose(fp);
    majorNumber = atoi(buf);
    printf("Major number: %d\n", majorNumber);
}

/*
 * Read from the device and print out the result.
 */
void readDev()
{
    char    localBuf[256];

    /* Read the device */
    memset(localBuf, 0, 256);
    if ( read(fileno(fd), localBuf, 256) == -1 )
    {
        printf("Failed to read device: %s: %s\n", DEVICE_NAME, strerror(errno));
        return;
    }

    printf("Device buffer (%d bytes): *%s*\n", (int)strlen(localBuf), localBuf);
}

/*
 * Write to the device an updated count.
 */
void writeDev()
{
    /* Build a buffer */
    sprintf(buf, "Write count: %d", ++cnt);

    /* Write the device */
    if ( write(fileno(fd), buf, strlen(buf)) == -1 )
    {
        printf("Failed to write device: %s: %s\n", DEVICE_NAME, strerror(errno));
        return;
    }
    printf("Wrote to device: Write count: %d\n", cnt);
}

/*
 * Use the ioctl to toggle driver state.
 */
void ioctlDev()
{
    int ret_val;

    if ( majorNumber == -1 )
        return;

    ret_val = ioctl(fileno(fd), IOCTL_RESET, 0);
    if ( ret_val != 0 )
    {
        printf("Failed to reset device: %s\n", DEVICE_NAME);
        return;
    }
}

int main (int argc, char **argv)
{
    int     opt;
    char    *cmd = NULL;
    char    *ptr;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, "t:")) != -1 )
    {   
        switch (opt)
        {   
            /* Test string, as in "rwrir" (read, write, read, ioctl, read). */
            case 't':
                cmd = optarg;
                break;
        }
    }

    /* Get the major number */
    readMajorNumber();

    /* Open the device */
    snprintf(buf, 256, "/dev/%s", DEVICE_NAME);
    fd = fopen(buf, "r+");
    if ( fd == NULL )
    {
        printf("Failed to open device: %s: %s\n", buf, strerror(errno));
        return(-1);
    }

    /* Clear local buffer */
    memset(buf, 0, 256);

    ptr = cmd;
    printf("cmd: %s\n", cmd);
    while (ptr && *ptr != '\0')
    {
        if ( *ptr == 'w' )
        {
            writeDev();
        }
        else if ( *ptr == 'r' )
        {
            readDev();
        }
        else if ( *ptr == 'i' )
        {
            ioctlDev();
        }
        ptr++;
    }

    /* Close device */
    fclose(fd);

    return(0);
}
