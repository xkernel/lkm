# Note:
# You must install the kernel development package before this module will compile.
# Install dkms to update the driver automatically with each new kernel update.
#
# Cross compiling:
# Set the environment variables as noted in this file.
# They can be edited in the Makefile or set on the command line.
# Use this command line:
#    make XCC=1
#
# When testing under a systemd-based distribution you can view kernel logs with
#    sudo journalctl -f _TRANSPORT=kernel
# ---------------------------------------------------------------------------------

PROG = unittest

# Where are the modules currently installed (kernel source or binary package tree)
MODDIR=none

# The kernel module.  This is set to tell the kernel source about our module
# when we compile.
obj-m += mjh.o

# If XCC is specified then we assume you're trying to cross compile the driver.
ifneq ($(XCC),)

# Set these variables to perform a cross compile
# XCC = <path to gcc for cross compiler toolchain - leave off "gcc">
# ARCH = <such as arm, mips, etc.>
# Examples:
# XGCC  = /home/mjhammel/src/ximba/raspberrypi3/bld/crosstool-ng-1.22.0.bld/install/bin/arm-unknown-linux-gnueabi-
# ARCH  = arm

# Changed to cross compiled kernel source
# Example:
# MODDIR = /home/mjhammel/src/ximba/raspberrypi3/bld/linux-rpi2-4.4.y/modules/lib/modules/4.4.23-v7+/build

# Build the cross compiler name.
SGCC=${XCC}gcc

# This runs the cross compile by first validating required arguments.
all: validate $(PROG)
	make -C $(MODDIR) ARCH=$(ARCH) CROSS_COMPILE=$(XCC) SUBDIRS=$(PWD) modules

validate:
	@if [ "$(ARCH)" == "" ]; then \
		echo "You must specify the ARCH to use for cross compiling."; \
		exit 1; \
	fi
	@if [ ! -d $(MODDIR) ]; then \
		echo "No such MODDIR: $(MODDIR)"; \
		exit 1; \
	fi
	@if [ ! -f "$(SGCC)" ]; then \
		echo "Can't find cross compiler. Did you specify XCC correctly:  XCC=$(XCC)."; \
		exit 1; \
	fi

$(PROG): $(PROG).c
	$(SGCC) -Wall $(PROG).c -o $(PROG)

else

# Default directory where kernel headers will be found.
# Change this if you are cross compiling to 
#     <path_to_kernel_src>/modules/lib/modules/<kernel_version>/build
MODDIR = /lib/modules/$(shell uname -r)/build/

# This is run is we are not cross compiling
all: $(PROG)
	make -C $(MODDIR) M=$(PWD) modules

$(PROG): $(PROG).c
	gcc -Wall $(PROG).c -o $(PROG)

endif

clean:
	make -C $(MODDIR) M=$(PWD) clean
	rm -f $(PROG)
