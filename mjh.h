/**
 * @file    mjh.h
 * @author  Michael J. Hammel
 * @date    12 June 2017
 * @version 0.1
 * @brief  Header for mjh kernel module
 */

#define DEVICE_NAME     "mjh"       /* As in /dev/<device_name> - there will only be one. */
#define CLASS_NAME      "mjh"       /* The character class name we'll identify with. */
#define MJH_IOC_MAGIC   'h'         /* A value to use with our ioctl's */

/* IOCTL commands */
#define IOCTL_RESET     _IO(MJH_IOC_MAGIC, 1)           /* Reset the driver */

