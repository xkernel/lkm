/**
 * @file    mjh.c
 * @author  Michael J. Hammel
 * @date    12 June 2017
 * @version 0.1
 * @brief  Sample kernel module for character device with the following features:
 * Logs to /var/log/kern.log;
 * Accepts kernel parameters at load time;
 * Provides a sysfs interface for reading and modifying kernel configuration;
 * Provides an ioctl interface;
 * Provides read/write interface;
 * Based on Derek Molloy's hello world module.
 * @see https://gitlab.com/mjhammel/samples/lkm
 * @see https://github.com/derekmolloy/exploringBB/blob/master/extras/kernel/hello/hello.c
 */

#include <linux/init.h>             /* Macros used to mark up functions e.g., __init __exit */
#include <linux/module.h>           /* Core header for loading LKMs into the kernel */
#include <linux/kernel.h>           /* Contains types, macros, functions for the kernel */
#include <linux/device.h>           /* To get major number, for example */
#include <linux/fs.h>               /* File system support in the kernel */
#include <linux/uaccess.h>          /* Required for the copy to user function */
#include <linux/mutex.h>            /* Required for the mutex functionality */

#include "mjh.h"                    /* Driver public header */

#define INIT_MSG "init"

/* 
 * ----------------------------------------------------------
 * Function prototypes
 * ----------------------------------------------------------
 */
static int      dev_open(struct inode *, struct file *);
static int      dev_release(struct inode *, struct file *);
static ssize_t  dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t  dev_write(struct file *, const char *, size_t, loff_t *);
static long     dev_ioctl(struct file *filep, unsigned int cmd, unsigned long arg);

/* 
 * ----------------------------------------------------------
 * Variables used by the module and the kernel
 * ----------------------------------------------------------
 */

static DEFINE_MUTEX(mjh_mutex);         /* Macro to declare a new mutex */

static char     *logname = "mjh";       /* Used as module parameter example */
static int      majorNumber = -1;       /* Auto-generated at init time. */
static char     message[256] = {0};     /* Memory for the string that is passed from userspace */
static short    size_of_message;        /* Used to remember the size of the string stored */
static int      numberOpens = 0;        /* Counts the number of times the device is opened */

static struct class*  mjhClass  = NULL; /* The device-driver class struct pointer */
static struct device* mjhDevice = NULL; /* The device-driver device struct pointer */

/* 
 * Mapping of file operations to their module functions.
 * There are other operations, but this particular device only needs these functions.
 * See 
 *  struct file_operations 
 * in /linux/fs.h for a complete list.
 */
static struct file_operations fops =
{
    .open           = dev_open,
    .read           = dev_read,
    .write          = dev_write,
    .release        = dev_release,
    .unlocked_ioctl = dev_ioctl,
};

/** 
 * @brief The LKM initialization function
 * The static keyword restricts the visibility of the function to within this C file. The __init
 * macro means that for a built-in driver (not a LKM) the function is only used at initialization
 * time and that it can be discarded and its memory freed up after that point.
 * @return returns 0 if successful
 */
static int __init mjh_init(void){

    /* Dynamically allocate a major number for the device */
    majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
    if (majorNumber<0)
    {
        printk(KERN_ALERT "%s failed to register a major number\n", logname);
        return majorNumber;
    }
    printk(KERN_INFO "%s: registered with major number %d.\n", logname, majorNumber);

    /* Register our driver class */
    mjhClass = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(mjhClass))
    {
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "Failed to register device class\n");
        return PTR_ERR(mjhClass);
    }
    printk(KERN_INFO "%s: device class registered correctly\n", logname);

    /* Register the device driver */
    mjhDevice = device_create(mjhClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
    if (IS_ERR(mjhDevice))
    {
        class_destroy(mjhClass);
        unregister_chrdev(majorNumber, DEVICE_NAME);
        printk(KERN_ALERT "%s: Failed to create the device\n", logname);
        return PTR_ERR(mjhDevice);
    }
    printk(KERN_INFO "%s: device class created correctly\n", logname);

    /* Initialize the mutex dynamically */
    mutex_init(&mjh_mutex);

    /* Initialize local message buffer */
    sprintf(message, "%s", INIT_MSG);
    size_of_message = strlen(message);

    /* Log that we've been properly setup */
    printk(KERN_INFO "%s has been initialized.\n", logname);
    return 0;
}

/** 
 * @brief The LKM cleanup function
 * Similar to the initialization function, it is static. The __exit macro notifies that if this
 * code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit mjh_exit(void){
    mutex_destroy(&mjh_mutex);                       /* destroy the dynamically-allocated mutex */
    device_destroy(mjhClass, MKDEV(majorNumber, 0)); /* remove the device */
    class_unregister(mjhClass);                      /* unregister the device class */
    class_destroy(mjhClass);                         /* remove the device class */
    unregister_chrdev(majorNumber, DEVICE_NAME);     /* unregister the major number */
    printk(KERN_INFO "%s has exited.\n", logname);
}


/* 
 * ----------------------------------------------------------
 * File Operation Functions
 * ----------------------------------------------------------
 */

/*
 * We only allow a single connection to this driver at a time.  Alternatively, 
 * we could use an array of mutexes to allow multiple connections.
 * Note that the mutex would need to be associated with the file object if we
 * used an array of mutexes.
 */
static int dev_open(struct inode *inodep, struct file *filep)
{
    /* Try to acquire the mutex (returns 0 on fail) */
    if(!mutex_trylock(&mjh_mutex))
    {
        printk(KERN_ALERT "%s: Device in use by another process", logname);
        return -EBUSY;
    }

    numberOpens++;
    printk(KERN_INFO "%s: Device has been opened %d time(s)\n", logname, numberOpens);
    return 0;
}

/*
 * The read operation copies back whatever is in a local message buffer to the caller's
 * buffer.  The local buffer is in kernel space and the caller's buffer is in user's space
 * so we use copy_to_user() to do the copy.
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset)
{
    int error_count = 0;

    printk(KERN_INFO "%s: local buffer: %s\n", logname, message);

    /* copy_to_user has the format ( * to, *from, size) and returns 0 on success */
    error_count = copy_to_user(buffer, message, size_of_message);

    if (error_count==0)
    {
        printk(KERN_INFO "%s: Sent %d characters to the user\n", logname, size_of_message);

        /* clear the position to the start and return 0 */
        // return (size_of_message=0); 
        return (0); 
    }
    else 
    {
        printk(KERN_INFO "%s: Failed to send %d characters to the user\n", logname, error_count);

        /* Failed -- return a bad address message (i.e. -14) */
        return -EFAULT;
    }
}

/*
 * The local message buffer is read/write, which means the caller can fill it with new 
 * data if they choose.  We could also use a separate write buffer to avoid conflict with 
 * the read buffer.
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset)
{
    sprintf(message, "%s(%zu letters)", buffer, len);  /* appending received string with its length */
    size_of_message = strlen(message);                 /* store the length of the stored message */

    printk(KERN_INFO "%s: Received %zu characters from the user\n", logname, len);
    return len;
}

/*
 * The release function handles freeing up the mutex.  It will be called whenever the close()
 * function is called from user space on the device descriptor.
 */
static int dev_release(struct inode *inodep, struct file *filep)
{
    mutex_unlock(&mjh_mutex);
    printk(KERN_INFO "%s: Device successfully closed\n", logname);
    return 0;
}

/*
 * The ioctl function handles resetting the local buffer.
 * These are generally frowned upon due to the old BKL-usage of the old ioctl API.
 * However, for simple state changes to a driver they work fine.
 * Note: this assumes unlocked_ioctl() is supported in the kernel.
 */
static long dev_ioctl(struct file *filep, unsigned int cmd, unsigned long arg)
{
    switch( cmd )
    {
        case IOCTL_RESET: 
            sprintf(message, "%s", INIT_MSG);
            printk(KERN_INFO "%s: Device successfully reset: %s\n", logname, message);
            break;
    }
    return 0;
}

/* 
 * ----------------------------------------------------------
 * Module Macros go at the end
 * ----------------------------------------------------------
 */

/** 
 * @brief A module must use the module_init() and module_exit() macros from linux/init.h, which
 * identify the initialization function at insertion time and the cleanup function (as
 * listed above)
 */
module_init(mjh_init);
module_exit(mjh_exit);

/* Modinfo settings */
MODULE_LICENSE("GPL");              /* < The license type -- this affects runtime behavior */
MODULE_AUTHOR("Michael J. Hammel"); /* < The author -- visible when you use modinfo */
MODULE_VERSION("0.1");              /* < The version of the module */
MODULE_DESCRIPTION("Sample Linux driver with multiple APIs.");  
                                    /* < The description -- see modinfo */
/* Module parameters */
module_param(logname, charp, S_IRUGO);  /* < Param desc. charp = char ptr, S_IRUGO can be read/not changed */
MODULE_PARM_DESC(logname, "The name to display in /var/log/kern.log"); 
                                        /* < parameter description */
module_param(majorNumber, int, S_IRUGO);/* < Param desc., S_IRUGO can be read/not changed */
MODULE_PARM_DESC(logname, "Device major number"); 
                                        /* < parameter description */
